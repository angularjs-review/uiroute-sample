'use strict';
/*
npm install gulp gulp-util browser-sync browser-sync-spa --save-dev
* */
// var path = require('path');
// var util = require('util');
// var conf = require('./conf');
// var browserSync = require('browser-sync').create();

var gulp = require('gulp');
var browserSync = require('browser-sync');
var browserSyncSpa = require('browser-sync-spa');

var src = './src';
var dist = './dist';
var paths = {
  js: src + '/**/*/js',
  css: src + '/**/*.css'
};


gulp.task('html', function(){
  return gulp
      .src('./**/*.html')
      .pipe(browserSync.reload({
          stream : true
      }));
});

gulp.task('js', function(){
  return gulp
      .src(paths.js)
      .pipe(gulp.dest(dist+'/js'))
      .pipe(gulp.dest(dist+'/js'))
      .pipe(browserSync.reload(
          {stream : true}
      ));
});

gulp.task('css', function(){
  return gulp
      .src(paths.css)
      .pipe(gulp.dest(dist+'/css'))
      .pipe(browserSync.reload(
          {stream: true}
      ));
});


browserSync.use(browserSyncSpa({
    selector: '[ng-app]'
}));

gulp.task('browserSync', ['html', 'js', 'css'], function(){
  return browserSync.init({
     port: 3030,
     server:{
       baseDir: './'
     }
  });
});

gulp.task('watch', function(){
  gulp.watch('./**/*.html', ['html']);
  gulp.watch(paths.js, ['js']);
  gulp.watch(paths.css, ['css']);
  browserSync.reload();
});

gulp.task('serve', ['browserSync', 'watch']);

// function browserSyncInit(baseDir, browser){
//     var routes = null;
//
//     if(baseDir === conf.paths.src || (util.isArray(baseDir) && baseDir.indexOf(conf.paths.src) !== -1)) {
//         routes = {
//             '/bower_components': 'bower_components'
//         };
//     }
//
//     var server = {
//         baseDir : baseDir,
//         routes: routes,
//         browser: browser,
//         open: false,
//         ghostMode: false
//     };
//
//     browserSync.instance = browserSyncSpa.init({
//         startPath:'/',
//         server: server,
//         browser: browser,
//         open: false,
//         ghostMode: false
//     });
// }

// gulp.task('serve', ['watch'], function(){
//   // browserSyncInit([path.join(conf.paths.tmp,'/serve'), conf.paths.src]);
// });

